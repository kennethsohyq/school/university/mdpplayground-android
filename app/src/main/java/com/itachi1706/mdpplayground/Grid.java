package com.itachi1706.mdpplayground;

import android.util.Log;

/**
 * Created by Kenneth on 30/8/2018.
 * for com.itachi1706.mdpplayground in MDPPlayground
 */
public class Grid {

    public static final int GRID_NONE = 0, GRID_OBSTACLE = 1, GRID_ROBOT = 2;
    private int MAX_COL = 15, MAX_ROW = 20;
    int[][] gridField;
    int playerLocation = -1;

    public Grid(int maxRows, int maxCols) {
        this.MAX_COL = maxCols;
        this.MAX_ROW = maxRows;
        this.gridField = new int[this.MAX_ROW][this.MAX_COL];
    }

    public void setGridValue(int cell, int val) {
        // Convert to 2D
        int row = cell / MAX_COL;
        int col = cell - (row * MAX_COL);
        setGridValue(row, col, val);
    }

    public void setGridValue(int row, int col, int val) {
        gridField[row][col] = val;
    }

    public int[][] getGridField() {
        return gridField;
    }

    public int getGridSize() {
        return MAX_COL * MAX_ROW;
    }

    public void setPlayer(int cell) {
        if (playerLocation != -1) {
            setGridValue(playerLocation, GRID_NONE);
            setGridValue(playerLocation + 1, GRID_NONE);
            setGridValue(playerLocation + MAX_COL, GRID_NONE);
            setGridValue(playerLocation + MAX_COL + 1, GRID_NONE);
        }
        setGridValue(cell, GRID_ROBOT);
        setGridValue(cell + 1, GRID_ROBOT);
        setGridValue(cell + MAX_COL, GRID_ROBOT);
        setGridValue(cell + MAX_COL + 1, GRID_ROBOT);
        playerLocation = cell;
        Log.d("Player", "Current Loc: " + cell);
    }

    public int getPlayer() {
        return playerLocation;
    }

    public int[] convert1Dto2D(int value1D) {
        int[] result = new int[2];
        result[0] = value1D / MAX_COL; // Row
        result[1] = value1D - (result[0] * MAX_COL);
        return result;
    }

    public int convert2Dto1D(int row, int col) {
        return row * MAX_COL + col;
    }

    public void resetBoard() {
        for (int i = 0; i < gridField.length; i++) {
            for (int j = 0; j < gridField[i].length; j++)
                gridField[i][j] = GRID_NONE;
        }
    }

    public boolean isObstacleOrWall(int row, int col) {
        // Check out of bounds
        Log.d("OBWALL", "Row: " + row + " Col: " + col);
        if (row < 0 || row >= MAX_ROW) return true;
        if (col < 0 || col >= MAX_COL) return true;
        return gridField[row][col] == GRID_OBSTACLE;
    }

    // Controls
    public boolean tryGoUp() {
        // Check for obstacles for 2x2 area
        // [x o]
        // [v o]
        int[] ppos = convert1Dto2D(playerLocation);
        if (isObstacleOrWall(ppos[0] - 1, ppos[1])) {
            return false;
        }
        if (isObstacleOrWall(ppos[0] - 1, ppos[1] + 1)) {
            return false;
        }
        if (isObstacleOrWall(ppos[0], ppos[1] + 1)) {
            return false;
        }
        // Move up (deduct row by 1)
        setPlayer(convert2Dto1D(ppos[0] - 1, ppos[1]));
        return true;
    }

    public boolean tryGoDown() {
        // [v o]
        // [x o]
        // [0 o]
        // Check for obstacles
        int[] ppos = convert1Dto2D(playerLocation);
        if (isObstacleOrWall(ppos[0] + 1, ppos[1])) {
            return false;
        }
        if (isObstacleOrWall(ppos[0] + 1, ppos[1] + 1)) {
            return false;
        }
        if (isObstacleOrWall(ppos[0] + 2, ppos[1] + 1)) {
            return false;
        }
        if (isObstacleOrWall(ppos[0] + 2, ppos[1])) {
            return false;
        }
        // Move down (add row by 1)
        setPlayer(convert2Dto1D(ppos[0] + 1, ppos[1]));
        return true;
    }

    public boolean tryGoLeft() {
        // Check for obstacles
        // [x v]
        // [o o]
        int[] ppos = convert1Dto2D(playerLocation);
        if (isObstacleOrWall(ppos[0], ppos[1] - 1)) {
            return false;
        }
        if (isObstacleOrWall(ppos[0] + 1, ppos[1] -1)) {
            return false;
        }
        if (isObstacleOrWall(ppos[0] + 1, ppos[1])) {
            return false;
        }
        // Move left (deduct col by 1)
        setPlayer(convert2Dto1D(ppos[0], ppos[1] - 1));
        return true;
    }

    public boolean tryGoRight() {
        // Check for obstacles
        // [v x o]
        // [o o o]
        int[] ppos = convert1Dto2D(playerLocation);
        if (isObstacleOrWall(ppos[0], ppos[1] + 1)) {
            return false;
        }
        if (isObstacleOrWall(ppos[0] + 1, ppos[1] + 1)) {
            return false;
        }
        if (isObstacleOrWall(ppos[0] + 1, ppos[1] + 2)) {
            return false;
        }
        if (isObstacleOrWall(ppos[0], ppos[1] + 2)) {
            return false;
        }
        // Move up (add col by 1)
        setPlayer(convert2Dto1D(ppos[0], ppos[1] + 1));
        return true;
    }
}
