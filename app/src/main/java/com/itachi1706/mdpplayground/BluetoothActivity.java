package com.itachi1706.mdpplayground;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class BluetoothActivity extends AppCompatActivity {

    RecyclerView btList;
    Switch btOn;
    Button btScan, btDiscover;

    BluetoothAdapter bluetoothAdapter;
    ArrayMap<String, BluetoothDevice> devices;
    StringRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        btScan = findViewById(R.id.bt_scan);
        btList = findViewById(R.id.bt_list);
        btOn = findViewById(R.id.bt_switch);
        btDiscover = findViewById(R.id.bt_discoverable);

        if (btList != null) {
            btList.setHasFixedSize(true);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            btList.setLayoutManager(linearLayoutManager);
            btList.setItemAnimator(new DefaultItemAnimator());

            devices = new ArrayMap<>();
            devices.put("No devices found", null);
            adapter = new StringRecyclerAdapter(devices.keySet());
            btList.setAdapter(adapter);

            adapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView title =  v.findViewById(R.id.text1);
                    connectToDevice(title.getText().toString());
                }
            });
        }

        // Check if bt is even a thing
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            new AlertDialog.Builder(this).setTitle("Not Supported").setMessage("Bluetooth is not supported on this device")
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).show();
            return;
        }

        btOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !bluetoothAdapter.isEnabled()) {
                    bluetoothAdapter.enable();
                    return;
                }
                if (!isChecked && bluetoothAdapter.isEnabled()) {
                    bluetoothAdapter.disable();
                }
            }
        });

        btDiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bluetoothAdapter.isDiscovering()) {
                    Intent discoverableIntent =
                            new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
                    startActivity(discoverableIntent);
                    Toast.makeText(getApplicationContext(),"Making Device Discoverable for 5 minutes",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanForDevices();
            }
        });

        // Register for broadcasts when a device is discovered.
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        registerReceiver(mReceiver, filter);
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("BT", "ACTION: " + action);
            if (action == null || action.isEmpty()) return;
            switch (action) {
                case BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    String deviceName = device.getName();
                    String deviceHardwareAddress = device.getAddress(); // MAC address

                    Log.d("BT", "ACTION FOUND: " + deviceName + " : " + deviceHardwareAddress);

                    devices.put(deviceName + " (" + deviceHardwareAddress + ")", device);
                    adapter.updateList(devices.keySet());
                    break;
                case BluetoothDevice.ACTION_BOND_STATE_CHANGED:
                    int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1);
                    int previousState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, -1);

                    Log.d("BT", "ACTION_BOND_STATE_CHANGED: state:" + state + ", previous:" + previousState);
                    break;
                case BluetoothAdapter.ACTION_SCAN_MODE_CHANGED:
                    int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.ERROR);

                    switch(mode){
                        case BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                            Log.d("BT", "Scan Mode: Connectable Discoverable");
                            break;
                        case BluetoothAdapter.SCAN_MODE_CONNECTABLE:
                            Log.d("BT", "Scan Mode: Connectable");
                            break;
                        case BluetoothAdapter.SCAN_MODE_NONE:
                            Log.d("BT", "Scan Mode: None");
                            break;
                    }
                    break;
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    final int state2 = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                    switch(state2) {
                        case BluetoothAdapter.STATE_OFF:
                            Log.d("BT", "Bluetooth Off");
                            break;
                        case BluetoothAdapter.STATE_TURNING_OFF:
                            Log.d("BT", "Bluetooth Turning Off");
                            break;
                        case BluetoothAdapter.STATE_ON:
                            Log.d("BT", "Bluetooth On");
                            break;
                        case BluetoothAdapter.STATE_TURNING_ON:
                            Log.d("BT", "Bluetooth Turning On");
                            break;
                    }
                    break;
                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    Toast.makeText(getApplicationContext(), "Found " + devices.size() + " devices", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    private void connectToDevice(String connectionString) {
        Log.d("BT-Try", "Clicked " + connectionString);
        if (bluetoothAdapter.isDiscovering()) bluetoothAdapter.cancelDiscovery();
        BluetoothDevice device = devices.get(connectionString);
        if (device == null) return;

        Log.d("BT-Try", "Attempting connection to " + device.getAddress());
        device.createBond();
    }

    private void scanForDevices() {
        if (bluetoothAdapter.isDiscovering()) bluetoothAdapter.cancelDiscovery();
        if (!bluetoothAdapter.isDiscovering() && bluetoothAdapter.startDiscovery()) {
            devices.clear();
            Toast.makeText(this, "Scanning for devices", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Don't forget to unregister the ACTION_FOUND receiver.
        unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_logs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logs:
                startActivity(new Intent(this, ViewLogsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
