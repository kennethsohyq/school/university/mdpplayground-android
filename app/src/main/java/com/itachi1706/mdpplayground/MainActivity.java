package com.itachi1706.mdpplayground;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.nfc.NfcAdapter;
import android.nfc.tech.NfcA;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.biometrics.BiometricPrompt;
import androidx.biometrics.FingerprintDialogFragment;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Random;
import java.util.concurrent.Executor;

import static com.itachi1706.mdpplayground.Grid.GRID_NONE;
import static com.itachi1706.mdpplayground.Grid.GRID_OBSTACLE;
import static com.itachi1706.mdpplayground.Grid.GRID_ROBOT;

public class MainActivity extends AppCompatActivity {

    GridLayout table;
    Button bUp, bDown, bLeft, bRight;
    ColorDrawable white, blue, red;
    private Grid playField;
    private SwitchCompat switchBtn;


    static final int MAX_COL = 15, MAX_ROW = 20;
    static final int MAX_ROBOTS = 1, MAX_OBSTACLES = 40;

    SensorEventListener gyroscopeSensorListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        table = findViewById(R.id.grid_field);

        // Add views into table
        white = new ColorDrawable(Color.parseColor("#FFFFFF"));
        blue = new ColorDrawable(Color.parseColor("#0000FF"));
        red = new ColorDrawable(Color.parseColor("#FF0000"));
        table.setRowCount(MAX_ROW);
        table.setColumnCount(MAX_COL);

        for (int i = 0; i < (MAX_COL*MAX_ROW); i++) {
            View v = getLayoutInflater().inflate(R.layout.cell, null);
            v.setLayoutParams(new LinearLayout.LayoutParams(70, 70));
            v.setBackground(red);
            table.addView(v);
        }
        playField = new Grid(MAX_ROW, MAX_COL);

        // Init grid
        Log.d("Grid", "Grid Size Start: " + playField.getGridSize());
        randomClearAndInsert();

        bUp = findViewById(R.id.up);
        bDown = findViewById(R.id.down);
        bLeft = findViewById(R.id.left);
        bRight = findViewById(R.id.right);

        bUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean works = playField.tryGoUp();
                Log.d("HALP LA", "MVMT UP: " + works);
                if (works) updateBoard();
            }
        });
        bDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean works = playField.tryGoDown();
                Log.d("HALP LA", "MVMT DOWN: " + works);
                if (works) updateBoard();
            }
        });
        bLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean works = playField.tryGoLeft();
                Log.d("HALP LA", "MVMT LEFT: " + works);
                if (works) updateBoard();
            }
        });
        bRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean works = playField.tryGoRight();
                Log.d("HALP LA", "MVMT RIGHT: " + works);
                if (works) updateBoard();
            }
        });


        // Create a listener
        gyroscopeSensorListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                // More code goes here
                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];
                float z = sensorEvent.values[2];
                Log.d("Gryo", String.format("X: %.2f Y: %.2f Z: %.2f", x, y, z));
                if (y > 0.5f) {
                    if (playField.tryGoDown()) {
                        updateBoard();
                    }
                } else if (y < -0.5f) {
                    if (playField.tryGoUp()) {
                        updateBoard();
                    }
                } else if (x > 0.5f) {
                    if (playField.tryGoLeft()) {
                        updateBoard();
                    }
                } else if (x < -0.5f) {
                    if (playField.tryGoRight()) {
                        updateBoard();
                    }
                }

                // +y - right
                // -y - left
                // +x - up
                // -x - down
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
            }
        };




        /*switchBtn = findViewById(R.id.test_toggle);
        switchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Switch is clicked", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    @Override
    public void onResume() {
        super.onResume();

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor gryo = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        // Register the listener
        //sensorManager.registerListener(gyroscopeSensorListener, gryo, 10);
        Log.d("Gryo", "Listener Registered");
    }

    @Override
    protected void onPause() {
        super.onPause();

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        //sensorManager.unregisterListener(gyroscopeSensorListener);
    }

    private void toggleBtnClicked(View v) {
        Toast.makeText(v.getContext(), "Btn clicked from XML", Toast.LENGTH_SHORT).show();
    }

    private void randomClearAndInsert() {
        // Clear board
        playField.resetBoard();
        int gridSize = playField.getGridSize();
        Log.d("Board", "Grid Size: " + gridSize);

        // Generate random location
        Random random = new Random();
        for (int i = 0; i < MAX_OBSTACLES; i++) {
            int cell = random.nextInt(gridSize);
            playField.setGridValue(cell, GRID_OBSTACLE);
        }

        playField.setPlayer(random.nextInt(gridSize));

        updateBoard();
    }

    private void updateBoard() {
        // Convert 2D array to 1D array
        int[] tmp = new int[playField.getGridSize()];
        int c = 0;
        for (int[] i : playField.getGridField()) {
            for (int j : i) {
                tmp[c] = j;
                c++;
            }
        }

        for (int i = 0; i < table.getChildCount(); i++) {
            ImageView cell = (ImageView) table.getChildAt(i);
            cell.setImageDrawable(getResources().getDrawable(R.drawable.border, null));
            switch (tmp[i]) {
                case GRID_OBSTACLE: cell.setBackground(blue); break;
                case GRID_ROBOT: cell.setBackground(red); break;
                case GRID_NONE:
                default: cell.setBackground(white); break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.random:
                randomClearAndInsert();
                return true;
            case R.id.test:
                new AlertDialog.Builder(this, R.style.AlertDialogStyle).setTitle("Test").setMessage("Test")
                        .setPositiveButton("CLOSE", null).show();
                return true;
            case R.id.bt:
                startActivity(new Intent(this, BluetoothActivity.class));
                return true;
            case R.id.logs:
                startActivity(new Intent(this, ViewLogsActivity.class));
                return true;
            case R.id.fptest:
                handleFP();
                return true;
            case R.id.nfc:
                doNFCScan();
                return true;
            case R.id.oauth:
                startActivity(new Intent(this, AuthActivity.class));
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Fingerprinting
     */

    private void handleFP() {
        Executor executor = new Executor() {
            @Override
            public void execute(Runnable command) {
                command.run();
            }
        };
        BiometricPrompt p = new BiometricPrompt(this, executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(), "Authentication Error (" + errString + ")", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Toast.makeText(getApplicationContext(), "Authentication Succeeded", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication Failed", Toast.LENGTH_LONG).show();
            }
        });
        BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder().setTitle("Autheticate")
                .setSubtitle("Check if you are not Audrey").setDescription("To authenticate, press your finger onto your fingerprint sensor")
                .setNegativeButtonText("Help La").build();

        p.authenticate(promptInfo);

    }

    private static final String TAG = "Main";

    private void doNFCScan() {
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            Log.e(TAG, "No NFC Adapter");
            Toast.makeText(this, "No NFC support on device", Toast.LENGTH_LONG).show();
            return;
        }

        if (!nfcAdapter.isEnabled()) {
            Log.e(TAG, "NFC Disabled");
            new AlertDialog.Builder(this).setTitle("NFC Disabled").setMessage("NFC is disabled. Enable to use").setPositiveButton("Close", null).show();
            return;
        }



        boolean mMifareClassicSupport = this.getPackageManager().hasSystemFeature("com.nxp.mifare");
        Log.d(TAG, "Falling back to com.nxp.mifare feature detection " + (mMifareClassicSupport ? "(found)" : "(missing)"));
    }
}
