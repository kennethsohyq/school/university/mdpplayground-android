package com.itachi1706.mdpplayground;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created by Kenneth on 9/9/2018.
 * for com.itachi1706.mdpplayground in MDPPlayground
 */
public class StringRecyclerAdapter extends RecyclerView.Adapter<StringRecyclerAdapter.StringViewHolder> {
    private List<String> stringList;
    private boolean announce = false;
    private View.OnClickListener onClickListener = null;

    public StringRecyclerAdapter(List<String> strings) {
        this(strings, true);
    }

    public StringRecyclerAdapter(Set<String> strings) {
        this.stringList = new ArrayList<>();
        this.stringList.addAll(strings);
        this.announce = true;
    }

    public StringRecyclerAdapter(String[] strings) {
        this(strings, true);
    }

    public StringRecyclerAdapter(List<String> strings, boolean announce)
    {
        this.stringList = strings;
        this.announce = announce;
    }

    public StringRecyclerAdapter(String[] strings, boolean announce)
    {
        this.stringList = Arrays.asList(strings);
        this.announce = announce;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        onClickListener = listener;
    }

    public void updateList(List<String> newString) {
        this.stringList = newString;
        notifyDataSetChanged();
    }

    public void updateList(Set<String> newString) {
        this.stringList.clear();
        this.stringList.addAll(newString);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        return stringList.size();
    }

    @Override
    public void onBindViewHolder(StringViewHolder stringViewHolder, int i)
    {
        String s  = stringList.get(i);
        stringViewHolder.title.setText(s);
    }

    @Override
    public StringViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.recyclerview_default_simple_list_item_1, viewGroup, false);

        return new StringViewHolder(itemView, onClickListener);
    }


    public class StringViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected TextView title;

        public StringViewHolder(View v, @Nullable View.OnClickListener listener)
        {
            super(v);
            title = v.findViewById(R.id.text1);
            v.setOnClickListener((listener == null) ? this : listener);
        }

        @Override
        public void onClick(View v) {
            if (announce) Toast.makeText(v.getContext(), title.getText(), Toast.LENGTH_SHORT).show();
        }

    }
}