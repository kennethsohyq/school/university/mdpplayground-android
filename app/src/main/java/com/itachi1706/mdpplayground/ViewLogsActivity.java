package com.itachi1706.mdpplayground;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ViewLogsActivity extends AppCompatActivity {

    TextView view;
    AsyncTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_logs);
        view = findViewById(R.id.log_view);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (task != null) {
            task.cancel(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (task == null)
            task = new LogReadingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log_act, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear:
                try {
                    Runtime.getRuntime().exec("logcat -c");
                    Toast.makeText(this, "Logs Cleared", Toast.LENGTH_LONG).show();
                    view.setText("");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private class LogReadingTask extends AsyncTask<Void, Void, Void> {

        LogReadingTask() {}

        private List<String> log;

        @Override
        protected Void doInBackground(Void... voids) {
            while (!isCancelled()) {
                try {
                    Process process = Runtime.getRuntime().exec("logcat -d -v threadtime");
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(process.getInputStream()));

                    if (log == null) log = new ArrayList<>();
                    log.clear();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        if (line.contains("IPerf::tryGetService failed!")) continue; // Ignore this cause this is annoying
                        log.add(line);
                    }
                    Collections.reverse(log);
                    StringBuilder sb = new StringBuilder();
                    for (String s : log) {
                        sb.append(s).append("\n");
                    }
                    final String present = sb.toString();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            view.setText(present);
                        }
                    });
                    for (int i = 0; i < 5; i++) {
                        Thread.sleep(1000);
                        if (isCancelled()) return null;
                    }
                } catch (IOException | InterruptedException ignored) {
                }
            }
            return null;
        }


    }
}
