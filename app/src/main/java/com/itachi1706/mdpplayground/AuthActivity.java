package com.itachi1706.mdpplayground;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.ResponseTypeValues;

import java.util.HashMap;
import java.util.Map;

public class AuthActivity extends AppCompatActivity {

    public static final String MY_REDIR_URL = "http://localhost:3001/callback", MY_CLIENTID = "TestDemo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        setupAuthRequest();

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginToSingpass(v);
            }
        });
    }

    public static AuthorizationServiceConfiguration configuration;
    private AuthorizationRequest.Builder builder;
    public static final int RC_AUTH = 3;

    private void setupAuthRequest() {
        configuration = new AuthorizationServiceConfiguration(
                Uri.parse("https://myinfosg.api.gov.sg/v2/authorise"),
                Uri.parse("https://myinfosg.api.gov.sg/v2/token")
        );

        builder = new AuthorizationRequest.Builder(
                configuration, MY_CLIENTID, ResponseTypeValues.CODE, Uri.parse(MY_REDIR_URL)
        );
    }

    private void loginToSingpass(View v) {

        Map<String, String> additional = new HashMap<>();
        additional.put("purpose", "Testing Purpose");
        additional.put("attributes", "name");
        AuthorizationService service = new AuthorizationService(this);
        builder.setScope("name");
        builder.setAdditionalParameters(additional);
        Intent auth = service.getAuthorizationRequestIntent(builder.build());
        startActivityForResult(auth, RC_AUTH);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_AUTH) {
            //AuthorizationResponse resp = AuthorizationResponse.fromIntent(data);
            //AuthorizationException ex = AuthorizationException.fromIntent(data);
            // ... process the response or exception ...
            Intent intent = new Intent(this, OAuthRedirectActivity.class);
            if (data.getExtras() != null)
                intent.putExtras(data.getExtras());
            intent.setData(data.getData());
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
