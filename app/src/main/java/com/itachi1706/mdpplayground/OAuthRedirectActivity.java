package com.itachi1706.mdpplayground;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.TokenRequest;
import net.openid.appauth.TokenResponse;

import java.util.Iterator;
import java.util.Set;

public class OAuthRedirectActivity extends AppCompatActivity {

    private AuthState instance;
    private AuthorizationResponse resp;
    private AuthorizationException ex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oauth_redirect);


        resp = AuthorizationResponse.fromIntent(getIntent());
        //ex = AuthorizationException.fromIntent(getIntent());
        instance = new AuthState(AuthActivity.configuration);
        //instance.update(resp, ex);
        Uri redirectUri = getIntent().getData();
        Log.d("Redir", redirectUri.toString());
        processUri(redirectUri);
        // ... process the response or exception ...

        /*if (resp != null) {
            obtainToken();
        } else {
            new AlertDialog.Builder(this).setTitle("Auth failed (" + ex.code + ")")
                    .setMessage("Auth failed: " + ex.error + "\n\nDetailed Description: " + ex.errorDescription)
                    .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).show();
        }*/
    }

    private void processUri(Uri uri) {
        Set<String> names = uri.getQueryParameterNames();
        StringBuilder sb = new StringBuilder();
        for (String name : names) {
            if (name.equalsIgnoreCase("code")) {
                authCode = uri.getQueryParameter(name);
            }
            sb.append(name).append(": ").append(uri.getQueryParameter(name)).append("\n");
        }

        TextView lol = findViewById(R.id.tv_accesstoken);
        lol.setText(sb.toString());

    }

    private String authCode;

    private void obtainToken() {
        AuthorizationService service = new AuthorizationService(this);

        TokenRequest tokenRequest = new TokenRequest.Builder(AuthActivity.configuration, AuthActivity.MY_CLIENTID)
                .setAuthorizationCode(authCode).setRedirectUri(Uri.parse(AuthActivity.MY_REDIR_URL)).build();
        service.performTokenRequest(tokenRequest, new AuthorizationService.TokenResponseCallback() {
            @Override
            public void onTokenRequestCompleted(@Nullable TokenResponse response, @Nullable AuthorizationException ex) {
                if (resp != null) {
                    doStuffWithAccessToken(response);
                } else {
                    exception(ex);
                }
            }
        });
    }

    private void exception(AuthorizationException ex) {
        new AlertDialog.Builder(getApplicationContext()).setTitle("Auth failed (" + ex.code + ")")
                .setMessage("Auth failed: " + ex.error + "\n\nDetailed Description: " + ex.errorDescription)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).show();
    }

    private void doStuffWithAccessToken(@Nullable TokenResponse response) {
        TextView at = findViewById(R.id.tv_accesstoken);
        TextView it = findViewById(R.id.tv_idtoken);
        at.setText("Access Token: " + response.accessToken);
        it.setText("ID Token: " + response.idToken);
    }
}
